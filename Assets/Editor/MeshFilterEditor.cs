﻿// using UnityEngine;
// using UnityEditor;

// using System.Collections.Generic;

// [CustomEditor(typeof(MeshFilter))]
// public class MeshFilterEditor : Editor
// {
//     public override void OnInspectorGUI()
//     {
// 		base.OnInspectorGUI();

// 		GUILayout.Label("Submeshes");

// 		MeshFilter meshFilter = (MeshFilter)target;
// 		Mesh mesh = meshFilter.sharedMesh;
//         if( !mesh )
//         {
// 			return;
// 		}

// 		GUILayout.Label("Count: " + mesh.subMeshCount);
// 		GUILayout.Label("Materials");

// 		Renderer renderer = meshFilter.GetComponentInParent<Renderer>();
// 		List<Material> materials = new List<Material>();
// 		if (renderer)
// 		{
// 			renderer.GetSharedMaterials(materials);
// 		}

// 		for (int i = 0; i < mesh.subMeshCount; ++i )
//         {
// 			Rect rect = GUILayoutUtility.GetRect(0.0f, 16.0f);
// 			EditorGUI.LabelField(rect, materials[i].name);
// 		}
// 	}
// }
