﻿using UnityEngine;
using System.Collections;

public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
{
	protected static T s_instance;

    protected bool m_bIsDestroyed = false;

    public bool isDestroyed
    {
        get { return m_bIsDestroyed; }
    }

	/**
	   Returns the instance of this singleton.
	*/
	public static T getInstance()
	{
		return s_instance;        
	}

	protected virtual void Awake()
	{
		if ( s_instance == null )
		{
            s_instance = this as T;
			DontDestroyOnLoad( this );
		}
		else if( this != s_instance )
		{
			Destroy( this.gameObject );			
		}
	}

    void OnDestroy()
    {
        m_bIsDestroyed = true;
    }
}
