﻿using UnityEngine;
using UnityEngine.SceneManagement;

using System.Collections;

public class InputManager : MonoBehaviour
{

    private static InputManager s_inputManager;    // static instance

	private Vector3 m_clickStartPosition = Vector3.zero;
	private Vector3 m_clickEndPosition = Vector3.zero;

	private Vector3 m_cutDirection;
	private bool m_readyToCut = false;

	[SerializeField]
	private LayerMask m_cuttableObjectLayermask = 0;

	private GameObject m_objectToCut = null;

	private float m_X = 0.0f, m_Y = 0.0f;

	[SerializeField]
	private float m_fMouseXSensitivity = 2.0f, m_fMouseYSensitivity = 0.5f;

    public static float X
    {
        get{ return s_inputManager.m_X; }
    }

	public static float Y
	{
		get { return s_inputManager.m_Y; }
	}

	void Awake()
    {
        if ( s_inputManager != null )
        {
            DestroyImmediate( this.gameObject );
        }
        else
        {
            DontDestroyOnLoad( this );
            s_inputManager = this;
        }

		Input.simulateMouseWithTouches = true;

        //Cursor.lockState = CursorLockMode.Locked;
        //Cursor.visible = false;
    }

    // Update is called once per frame
    void Update()
    {
        if( Input.GetKeyDown(KeyCode.Escape))
        {
			Application.Quit();
		}

        if( Input.GetKeyDown(KeyCode.Space))
        {
			SceneManager.LoadScene(0);
		}

        if( Input.GetMouseButton(0) )
        {
            if( m_objectToCut == null )
            {
				Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
				RaycastHit hit;

				if (Physics.Raycast(ray, out hit, 100, m_cuttableObjectLayermask))
				{
					m_objectToCut = hit.transform.gameObject;
					m_clickStartPosition = hit.point;
				}
            }
            else if( !m_readyToCut )
            {
				Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
				RaycastHit hit;

				bool finished = false;
				if (Physics.Raycast(ray, out hit, 100, m_cuttableObjectLayermask))
				{
                    if( hit.transform != m_objectToCut.transform )
					{
						finished = true;
					}
				}
                else
                {
					finished = true;
				}

                if( finished )
                {
					m_readyToCut = true;
					m_clickEndPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
					m_cutDirection = ray.direction;
                }
            }
        }
        else if (Input.GetMouseButtonUp(0))
		{
			if (m_readyToCut)
			{
				bool fill = true;
				bool addRigidbody = true;
				// Plane plane = new Plane(m_clickStartPosition, Camera.main.transform.position, m_clickEndPosition);
				Vector3 point = new Vector3((m_clickStartPosition.x + m_clickEndPosition.x) / 2, (m_clickStartPosition.y + m_clickEndPosition.y) / 2, (m_clickStartPosition.z + m_clickEndPosition.z) / 2);
				Cutter.Cut(m_objectToCut, point, Vector3.Cross(m_clickEndPosition - m_clickStartPosition, m_cutDirection).normalized, m_objectToCut.GetComponent<Renderer>().material, fill, addRigidbody );

				Plane plane = new Plane(m_objectToCut.transform.InverseTransformDirection(Vector3.Cross(m_clickEndPosition - m_clickStartPosition, m_cutDirection).normalized), // Transforms a direction from world space to local space - used here as the normal
								m_objectToCut.transform.InverseTransformPoint(m_clickStartPosition)); // Transforms a point from world space to local space

			}

			m_objectToCut = null;
			m_readyToCut = false;
		}

		if ( Input.GetMouseButtonDown( 0 ) ||
             Input.GetKeyDown(KeyCode.E) )
        {
            EventManager.triggerEvent( Hand.TOGGLEHOLD );
        }

        if ( Input.GetMouseButtonDown( 1 )||
			Input.GetKeyDown(KeyCode.UpArrow) ||
			Input.GetKeyDown(KeyCode.DownArrow) )
        {
            Debug.Log( "Mouse button 1 down" );
            EventManager.triggerEvent( ArmControl.MOVEUPANDDOWN );
        }
        else if( Input.GetMouseButtonUp( 1 )||
			Input.GetKeyUp(KeyCode.UpArrow) ||
			Input.GetKeyUp(KeyCode.DownArrow) )
        {
            EventManager.triggerEvent( ArmControl.CLEARMOVEUPANDDOWN );
		}

		if (Input.GetMouseButtonDown(2) ||
			Input.GetKeyDown(KeyCode.D) ||
			Input.GetKeyDown(KeyCode.A))
		{
			Debug.Log("Mouse button 2 down");
			EventManager.triggerEvent(ArmControl.MOVELEFTANDRIGHT);
		}
		else if (Input.GetMouseButtonUp(2) ||
			Input.GetKeyUp(KeyCode.D) ||
			Input.GetKeyUp(KeyCode.A))
        {
            EventManager.triggerEvent( ArmControl.CLEARMOVELEFTANDRIGHT );
        }

        if ( Input.GetKeyDown( KeyCode.Q ) )
        {

        }
        else if ( Input.GetKeyDown( KeyCode.W ) )
        {

        }

		UpdateXY();
	}

    void UpdateXY()
    {
		if ( Input.touchCount > 0 /*&& Input.GetTouch(0).phase != TouchPhase.Ended*/)
		{
			m_X = Input.GetTouch(0).deltaPosition.x;
			m_Y = Input.GetTouch(0).deltaPosition.y;
		}
		else
		{
			m_X = Input.GetAxis("Mouse X");
			m_Y = Input.GetAxis("Mouse Y");
		}

		if (Input.GetKey(KeyCode.RightArrow) ||
            Input.GetKey(KeyCode.D))
		{
			m_X = 0.5f;
		}
		else if (Input.GetKey(KeyCode.LeftArrow) ||
			Input.GetKey(KeyCode.A))
		{
			m_X = -0.5f;
		}

		if (Input.GetKey(KeyCode.UpArrow)||
            Input.GetKey(KeyCode.W))
		{
			m_Y = 0.5f;
		}
		else if (Input.GetKey(KeyCode.DownArrow) ||
			Input.GetKey(KeyCode.S))
		{
			m_Y = -0.5f;
		}

		m_X *= m_fMouseXSensitivity;
		m_Y *= m_fMouseYSensitivity;
	}

	void OnDrawGizmos()
	{
		if (m_readyToCut)
		{
			Vector3 point = new Vector3((m_clickStartPosition.x + m_clickEndPosition.x) / 2, (m_clickStartPosition.y + m_clickEndPosition.y) / 2, (m_clickStartPosition.z + m_clickEndPosition.z) / 2);
			Quaternion rotation = Quaternion.LookRotation(transform.TransformDirection(Vector3.Cross(m_clickEndPosition - m_clickStartPosition, m_cutDirection).normalized));
			Matrix4x4 trs = Matrix4x4.TRS(transform.TransformPoint(point), rotation, Vector3.one);
			Gizmos.matrix = trs;
			Color32 color = Color.blue;
			color.a = 125;
			Gizmos.color = color;
			Gizmos.DrawCube(Vector3.zero, new Vector3(1.0f, 1.0f, 0.0001f));
			Gizmos.matrix = Matrix4x4.identity;
			Gizmos.color = Color.white;
		}
	}

    void OnGUI()
    {
		if (Debug.isDebugBuild)
		{
			GUI.Label(new Rect(0, 0, 200, 25), "Mouse Position X: " + Input.mousePosition.x);
			GUI.Label(new Rect(0, 25, 200, 25), "Horizontal: " + Input.GetAxis("Horizontal"));
			GUI.Label(new Rect(0, 50, 200, 25), "X: " + m_X);

			GUI.Label(new Rect(0, 75, 200, 25), "Mouse Position Y: " + Input.mousePosition.y);
			GUI.Label(new Rect(0, 100, 200, 25), "Vertical: " + Input.GetAxis("Vertical"));
			GUI.Label(new Rect(0, 125, 200, 25), "Y: " + m_Y);
		}
	}
}