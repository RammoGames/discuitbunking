﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class Loader
{
    protected List<T> loadAllAssetsInFolder<T>( string path ) where T : UnityEngine.Object
    {
        List<T> objects = new List<T>();
        if ( typeof( T ) == typeof( Mesh ) )
        {
            List<GameObject> gameObjects = loadAllAssetsInFolder<GameObject>( path );
            foreach ( GameObject gameObject in gameObjects )
            {
                T tempObject = GameObject.Instantiate( gameObject.GetComponent<MeshFilter>().mesh ) as T;
                tempObject.name = gameObject.name.ToLower(); // make everything lowercase
                objects.Add( tempObject );
            }
        }
        else
        {
            T[] tempObjects = Resources.LoadAll<T>( path );
            if ( tempObjects == null || tempObjects.Length == 0 )
            {
                Debug.LogWarning( "No " + typeof( T ) + "s found to load" );
                //Debug.Break();
            }
            else
            {
                foreach( T obj in tempObjects )
                {
                    obj.name = obj.name.ToLower(); // make everything lowercase
                    objects.Add( obj );
                }
            }
        }

        return objects;
    }

    protected T loadAsset<T>( string path ) where T : UnityEngine.Object
    {
        T obj = Resources.Load( path ) as T;
        if ( obj == null )
        {
            Debug.LogError( "Can't load " + path );
            Debug.Break();
        }

        return obj;
    }

    protected GameObject loadGameObject( string path )
    {
        GameObject obj = Resources.Load( path ) as GameObject;
        if ( obj == null )
        {
            Debug.LogError( "Can't load " + path );
            Debug.Break();
        }

        return obj;
    }

    protected TextAsset loadTextAsset( string path )
    {
        TextAsset textAsset = (TextAsset)Resources.Load( path, typeof( TextAsset ) );
        if ( textAsset == null )
        {
            Debug.LogError( "Can't load " + path );
            Debug.Break();
        }

        return textAsset;
    }

    protected Mesh loadMeshAsset( string path )
    {
        Mesh mesh = (Mesh)Resources.Load( path, typeof( Mesh ) );
        if ( mesh == null )
        {
            Debug.LogError( "Can't load " + path );
            Debug.Break();
        }

        return mesh;
    }

    protected Material loadMaterialAsset( string path )
    {
        Material mesh = (Material)Resources.Load( path, typeof( Material ) );
        if ( mesh == null )
        {
            Debug.LogError( "Can't load " + path );
            Debug.Break();
        }

        return mesh;
    }
}
