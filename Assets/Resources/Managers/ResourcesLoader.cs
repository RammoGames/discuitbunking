﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Linq;

public class ResourcesLoader : Loader
{
    private static ResourcesLoader  s_resourcesLoader = new ResourcesLoader();

    private List<GameObject>        m_lGameObjects;
    private List<Mesh>              m_lMeshes;
    private List<Material>          m_lMaterials;
    private List<Texture2D>         m_lSprites;
    private List<GameObject>        m_lUIGameObjects;

    private List<TextAsset>         m_lLevels;
    private List<TextAsset>         m_lPlayers;
	
	// Use this for initialization
    private ResourcesLoader()
    {
        m_lLevels = new List<TextAsset>();
		loadResources();
	}

    public static ResourcesLoader getInstance()
    {
        if( s_resourcesLoader == null )
        {
            s_resourcesLoader = new ResourcesLoader();
        }

        return s_resourcesLoader;
    }

    public TextAsset getLevelData( int index )
    {
        return m_lLevels[ index ];
    }
	
	private void loadResources()
	{
        m_lGameObjects      = loadAllAssetsInFolder<GameObject>( "Prefabs" );
        m_lMeshes           = loadAllAssetsInFolder<Mesh>( "Meshes" );
        m_lMaterials        = loadAllAssetsInFolder<Material>( "Materials" );
        m_lSprites          = loadAllAssetsInFolder<Texture2D>( "Sprites" );
        m_lUIGameObjects    = loadAllAssetsInFolder<GameObject>( "Prefabs/UI" );

        // made this a public function so that we can reload the levels after creating a new one
        loadLevelData();

        // players
        m_lPlayers          = loadAllAssetsInFolder<TextAsset>( "Player/Players" );
	}

    public void loadLevelData()
    {
        // levels
        m_lLevels           = loadAllAssetsInFolder<TextAsset>( "Levels" );
    }

    public GameObject findGameObject( string name )
    {
        GameObject matchingGameObject = m_lGameObjects.Find( x => x.name.ToLower() == name.ToLower() );
        if( matchingGameObject == null )
        {
            Debug.LogError( "We don't have " + typeof( GameObject ).ToString() + ": " + name );
            Debug.Break();
        }

        return matchingGameObject;
    }

    public Mesh findMesh( string name )
    {
        Mesh matchingMesh = m_lMeshes.Find( x => x.name.ToLower() == name.ToLower() );
        if ( matchingMesh == null )
        {
            Debug.LogError( "We don't have " + typeof( Mesh ).ToString() + ": " + name );
            Debug.Break();
        }

        return matchingMesh;
    }

    public Material findMaterial( string name )
    {
        Material matchingMaterial = m_lMaterials.Find( x => x.name.ToLower() == name.ToLower() );
        if ( matchingMaterial == null )
        {
            Debug.LogError( "We don't have " + typeof( Material ).ToString() + ": " + name );
            Debug.Break();
        }

        return matchingMaterial;
    }

    public Texture2D findTexture( string name )
    {
        Texture2D matchingTexture = m_lSprites.Find( x => x.name.ToLower() == name.ToLower() );
        if ( matchingTexture == null )
        {
            Debug.LogError( "We don't have " + typeof( Texture2D ).ToString() + ": " + name );
            Debug.Break();
        }

        return matchingTexture;
    }

    public GameObject findUIGameObject( string name )
    {
        GameObject matchingUIGameObject = m_lUIGameObjects.Find( x => x.name.ToLower() == name.ToLower() );
        if ( matchingUIGameObject == null )
        {
            Debug.LogWarning( "We don't have UI GameObject: " + name );
            Debug.Break();
        }

        return matchingUIGameObject;
    }

    public TextAsset findLevelAsset( string name )
    {
        TextAsset matchingLevelAsset = m_lLevels.Find( x => x.name.ToLower() == name.ToLower() );
        if ( matchingLevelAsset == null )
        {
            Debug.LogWarning( "We don't have Level: " + name );
        }

        return matchingLevelAsset;
    }

    public TextAsset findPlayerAsset( string name )
    {
        TextAsset matchingPlayerAsset = m_lPlayers.Find( x => x.name.ToLower() == name.ToLower() );
        if ( matchingPlayerAsset == null )
        {
            Debug.LogWarning( "We don't have Player: " + name );
            Debug.Break();
        }

        return matchingPlayerAsset;
    }

    private int getIntValue( string str )
    {
        return System.Convert.ToInt32( str );
    }
}
