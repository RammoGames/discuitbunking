﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;

public class EventManager 
{
    public class IntEvent : UnityEvent<int> { } //empty class; just needs to exist

    private static EventManager s_eventManager;

    private Dictionary<string, UnityEvent> m_eventDictionary;
    //private Dictionary<string, IntEvent> m_intEventDictionary;

    public static EventManager getInstance()
    {
        if ( s_eventManager == null )
        {
            s_eventManager = new EventManager();
        }

        return s_eventManager;
    }

	// Use this for initialization
	EventManager() 
    {
	    if( m_eventDictionary == null )
        {
            m_eventDictionary = new Dictionary<string, UnityEvent>();
        }
	}
	
    public static void startListening( string eventName, UnityAction listener )
    {
        UnityEvent thisEvent = null;

        if ( getInstance().m_eventDictionary.TryGetValue( eventName, out thisEvent ) )
        {
            thisEvent.AddListener( listener );
        }
        else
        {
            // don't already have an event with this name
            thisEvent = new UnityEvent();
            thisEvent.AddListener( listener );
            getInstance().m_eventDictionary.Add( eventName, thisEvent );
        }
    }

    //public static void startListening<T>( string eventName, T listener ) where T : UnityAction
    //{
    //    if ( typeof( T ) == typeof( UnityAction<int> ) )
    //    {
    //        IntEvent thisEvent = null;
    //        if ( findEvent<IntEvent>( eventName, out thisEvent ) )
    //        {
    //            thisEvent.AddListener( listener as UnityAction<int> );
    //        }
    //        else
    //        {
    //            // don't already have an event with this name
    //            thisEvent = new IntEvent();
    //            thisEvent.AddListener( listener as UnityAction<int> );
    //            addEvent<IntEvent>( eventName, thisEvent );
    //            //getInstance().m_intEventDictionary.Add( eventName, thisEvent );
    //        }
    //    }
    //}

    //private static bool findEvent<T>( string eventName, out T thisEvent ) where T : UnityEvent
    //{
    //    if ( typeof( T ) == typeof( IntEvent ) )
    //    {
    //        return getInstance().m_intEventDictionary.TryGetValue( eventName, out thisEvent );
    //    }
    //}

    //private static void addEvent<T>( string eventName, T thisEvent )
    //{
    //    if ( typeof( T ) == typeof( UnityEvent ) )
    //    {
    //        getInstance().m_eventDictionary.Add( eventName, thisEvent );
    //    }

    //    if ( typeof( T ) == typeof( IntEvent ) )
    //    {
    //        getInstance().m_intEventDictionary.Add( eventName, thisEvent );
    //    }
    //}

    public static void stopListening( string eventName, UnityAction listener )
    {
        if( s_eventManager == null )
        {
            return; // no event manager
        }

        UnityEvent thisEvent = null;
        if ( getInstance().m_eventDictionary.TryGetValue( eventName, out thisEvent ) )
        {
            thisEvent.RemoveListener( listener );
        }
    }

    public static void triggerEvent( string eventName )
    {
        UnityEvent thisEvent = null;

        if ( getInstance().m_eventDictionary.TryGetValue( eventName, out thisEvent ) )
        {
            thisEvent.Invoke();
        }
    }
}
