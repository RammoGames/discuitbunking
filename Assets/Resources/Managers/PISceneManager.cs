﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

using System.Collections;
using System.Collections.Generic;

public class PISceneManager : Singleton<PISceneManager>
{
    // labels for the event manager
    public const string LOADPERFECTINSERT   = "loadPerfectInsert";
    public const string LOADGAMEONE         = "loadGameOne";
    public const string FINDGRABBABLES      = "findGrabbables";

    private UnityAction m_loadPerfectInsert;
    private UnityAction m_loadGameOne;
    private UnityAction m_findGrabbables;

    public Camera m_mainCamera              = null;
    public ArmControl m_armControl          = null;

    private Grabbable[] m_aGrabbables;

    const float TRANSITION_TIME             = 2.0f;
    const float TRANSITION_SMOOTH           = 2.0f;

    public Grabbable[] grabbables
    {
        get { return m_aGrabbables; }
    }

    struct GrabbableData
    {
        public GrabbableData( string name, Vector3 position, Quaternion rotation, Vector3 angularVelocity, Vector3 linearVelocity, bool isEnabled )
        {
            m_name              = name;
            m_position          = position;
            m_rotation          = rotation;
            m_linearVelocity    = linearVelocity;
            m_angularVelocity   = angularVelocity;
            m_isEnabled         = isEnabled;
        }

        public string name
        {
            get { return m_name; }
        }

        public Vector3 position
        {
            get { return m_position; }
        }

        public Quaternion rotation
        {
            get { return m_rotation; }
        }

        public Vector3 linearVelocity
        {
            get { return m_linearVelocity; }
        }

        public Vector3 angularVelocity
        {
            get { return m_angularVelocity; }
        }

        public bool isEnabled
        {
            get { return m_isEnabled; }
        }

        string      m_name;
        Vector3     m_position;
        Quaternion  m_rotation;
        Vector3     m_linearVelocity;
        Vector3     m_angularVelocity;
        bool        m_isEnabled;
    }

    private static List<GrabbableData> s_grabbableData;

    void Start()
    {
        m_loadPerfectInsert = new UnityAction(loadPerfectInsertScene);
		m_loadGameOne = new UnityAction(loadGameOne);
		m_findGrabbables = new UnityAction(findGrabbables);

		findGrabbables();
    }

    void OnEnable()
    {
        EventManager.startListening( LOADPERFECTINSERT, m_loadPerfectInsert );
        EventManager.startListening( LOADGAMEONE, m_loadGameOne );
        EventManager.startListening( FINDGRABBABLES, m_findGrabbables );
    }

    void OnDisable()
    {
        EventManager.stopListening( LOADPERFECTINSERT, m_loadPerfectInsert );
        EventManager.stopListening( LOADGAMEONE, m_loadGameOne );
        EventManager.stopListening( FINDGRABBABLES, m_findGrabbables );
    }

    private void findGrabbables()
    {
        Debug.Log( "Finding Grabbables" );
        m_aGrabbables = null;
        m_aGrabbables = FindObjectsOfType<Grabbable>();
    }

    public void savePerfectInsertScene()
    {
        s_grabbableData = new List<GrabbableData>();
        Grabbable[] grabbableObjects = MonoBehaviour.FindObjectsOfType<Grabbable>();
        foreach( Grabbable grabbable in grabbableObjects )
        {
            saveGrabbable( grabbable );
        }
    }

    private void saveGrabbable( Grabbable grabbable )
    {
        GrabbableData data = new GrabbableData( grabbable.name, grabbable.transform.position, grabbable.transform.rotation, grabbable.GetComponent<Rigidbody>().angularVelocity, grabbable.GetComponent<Rigidbody>().velocity, grabbable.enabled );
        s_grabbableData.Add( data );
    }

    public void loadPerfectInsertScene()
    {
        EventManager.triggerEvent( PISceneManager.FINDGRABBABLES );
        Grabbable[] grabbableObjects = MonoBehaviour.FindObjectsOfType<Grabbable>();
        foreach( Grabbable grabbable in grabbableObjects )
        {
            foreach( GrabbableData data in s_grabbableData )
            {
                if( grabbable.name == data.name )
                {
                    Debug.Log( "Grabbable " + grabbable.name + " found!" );
                    grabbable.transform.position                        = data.position;
                    grabbable.transform.rotation                        = data.rotation;
                    grabbable.GetComponent<Rigidbody>().velocity        = data.linearVelocity;
                    grabbable.GetComponent<Rigidbody>().angularVelocity = data.angularVelocity;
                    grabbable.enabled                                   = data.isEnabled;
                }
            }

            if( !grabbable.enabled )
            {
                DestroyImmediate( grabbable.gameObject );
            }
        }

        
    }
    
    private void loadPerfectInsert()
    {
		SceneManager.LoadScene(0);
    }

    private void loadGameOne()
    {
        savePerfectInsertScene();
        StartCoroutine( moveCameraToTV( 1 ) );
        StartCoroutine( moveArmBackwards() );
    }

    IEnumerator moveCameraToTV( int levelIndex )
    {
        if( m_mainCamera == null )
        {
            m_mainCamera = Camera.main;
        }

        CameraFollow cameraFollow = m_mainCamera.GetComponent<CameraFollow>();
        if( cameraFollow != null )
        {
            // remove the component so we don't try and follow the arm anymore
            Destroy( cameraFollow );
        }

        Vector3 targetPosition = new Vector3( 0.2f, 1.6f, 3.2f );
        float time = 0.0f;

        while ( time < TRANSITION_TIME )
        {
            time += Time.deltaTime * ( Time.timeScale / TRANSITION_SMOOTH );

            m_mainCamera.transform.position = Vector3.Lerp( m_mainCamera.transform.position, targetPosition, time );
            m_mainCamera.transform.rotation = Quaternion.Lerp( m_mainCamera.transform.rotation, Quaternion.identity, time );
            yield return null;
        }

        Debug.Log( "Loading level " + levelIndex + "!" );
		SceneManager.LoadScene(levelIndex);
	}

    IEnumerator moveArmBackwards()
    {
        Vector3 targetPosition = new Vector3( 0.0f, 0.7f, -10.0f );
        float time = 0.0f;

        while ( time < TRANSITION_TIME )
        {
            time += Time.deltaTime * ( Time.timeScale / TRANSITION_SMOOTH );

            m_armControl.transform.position = Vector3.Lerp( m_armControl.transform.position, targetPosition, time );
            yield return null;
        }
    }
}

