﻿using UnityEngine;

using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(Rigidbody))]
public class Grabbable : MonoBehaviour {

    private Collider m_colliderToIgnore;

    private bool m_bGrabbed = false;
	private bool m_addSpring = false, m_addLimits = false, m_degradeHingeOverTime = false;
	private float m_breakForce = 0.0f, m_breakTorque = 0.0f;
	private Transform m_connectedBody;
	private Vector3 m_gripPoint, m_hitPoint, m_newPosition; // m_hitpoint & m_newPosition used for debugging
	private Vector3 m_axis;

	private float m_hingeDegradeSpeed = 1.0f;

	[SerializeField]
	private List<Transform> m_gripPoints = null;

	public struct GrabbableConfig
	{
		public bool addHinge;
		public float breakForce;
		public float breakTorque;
		public bool addSpring;
		public bool addLimits;
		public bool degradeHingeOverTime;
	}

	// Use this for initialization
	void Start()
    {
        if ( m_bGrabbed )
        {
            setCollidersToIgnore();
        }
    }

    void Update()
    {
		if (m_degradeHingeOverTime)
        {
			HingeJoint hingeJoint = this.GetComponent<HingeJoint>();
			if (hingeJoint != null)
			{
				hingeJoint.breakForce -= Time.deltaTime * m_hingeDegradeSpeed;
				hingeJoint.breakTorque -= Time.deltaTime * m_hingeDegradeSpeed;
			}
        }
    }

    private void setCollidersToIgnore()
    {
        if ( m_colliderToIgnore == null )
        {
            Debug.Log( "We haven't got a collider to ignore..." );
        }
        else
        {
            Physics.IgnoreCollision( m_colliderToIgnore, this.GetComponent<Collider>() );
        }
    }

    private void clearCollidersToIgnore()
    {
        if ( m_colliderToIgnore != null )
        {
            Physics.IgnoreCollision( m_colliderToIgnore, this.GetComponent<Collider>(), false );
            m_colliderToIgnore = null;
        }
    }

    public void letGo()
    {
        HingeJoint hingeJoint = this.GetComponent<HingeJoint>();
        if ( hingeJoint != null )
        {
            DestroyImmediate( hingeJoint );
        }

        this.transform.parent = null;

		// make sure we're using gravity at least once it's been let go
		this.transform.GetComponent<Rigidbody>().useGravity = true;

		this.transform.GetComponent<Collider>().isTrigger = false;

        clearCollidersToIgnore();
    }

	private void UpdateGripPoints()
	{
		Collider collider = this.transform.GetComponent<Collider>();
		for (int i = m_gripPoints.Count; i-- > 0;) // go backwards so we can delete if we have to
		{
			Transform gripPoint = m_gripPoints[i];
			if (!collider.bounds.Contains(gripPoint.position))
			{
				m_gripPoints.Remove(gripPoint);
				DestroyImmediate(gripPoint.gameObject);
			}
		}
	}

	public IEnumerator GrabWithHand(Transform connectedBody, Vector3 hitPoint, Vector3 axis, GrabbableConfig config)
	{
		if (config.addHinge)
		{
			UpdateGripPoints();

			m_gripPoint = m_hitPoint = hitPoint;
			m_axis = axis;
			if (m_gripPoints != null && m_gripPoints.Count > 0)
			{
				// find the closest grip point and move us to it
				float closestDistance = float.MaxValue;
				Vector3 closestGripPoint = Vector3.zero;
				Vector3 closestAxis = axis;
				foreach (Transform tempGripPoint in m_gripPoints)
				{
					Vector3 point = tempGripPoint.position;
					float distance = Vector3.Distance(m_gripPoint, point);
					if (distance < closestDistance)
					{
						closestGripPoint = point;
						closestDistance = distance;
						closestAxis = tempGripPoint.right;
					}
				}

				if (closestGripPoint != null)
				{
					m_axis = closestAxis;

					Vector3 offset = closestGripPoint - hitPoint; // work out how far we need to move this object so that the gripPoint is in the right place
					m_newPosition = this.transform.position - offset; // this object's new position
					m_gripPoint = connectedBody.transform.position; // update the grip point

					this.transform.GetComponent<Rigidbody>().position = m_newPosition;

					yield return null; // give the rigidbody a chance to move before doing anything else
				}
			}
		}

		setupGrab(connectedBody, config);
	}

	private void CreateHinge()
	{
		HingeJoint hingeJoint = this.GetComponent<HingeJoint>();
		if (hingeJoint != null)
		{
			Destroy(hingeJoint);
			hingeJoint = null;
		}

		hingeJoint = gameObject.AddComponent<HingeJoint>();
		if (hingeJoint == null)
		{
			Debug.Log("Failed to add hinge joint to cartridge");
		}

		hingeJoint.connectedBody = m_connectedBody.GetComponent<Rigidbody>();
		hingeJoint.anchor = this.transform.InverseTransformPoint(m_gripPoint); // get the point relative to this object
		hingeJoint.axis = m_axis;
		hingeJoint.connectedAnchor = m_connectedBody.InverseTransformPoint(m_gripPoint); // get the point relative to the connected object
		hingeJoint.breakForce = m_breakForce * this.GetComponent<Rigidbody>().mass;
		hingeJoint.breakTorque = m_breakTorque * this.GetComponent<Rigidbody>().mass;
		hingeJoint.enableCollision = true;
		hingeJoint.enablePreprocessing = true;

		if (m_addSpring)
		{
			// add and enable a spring
			JointSpring hingeSpring = hingeJoint.spring;
			hingeSpring.spring = 50;
			hingeSpring.damper = 30;

			// float angle = Mathf.Acos( Vector3.Dot(this.transform.up.normalized, Vector3.forward.normalized) ) * Mathf.Rad2Deg;

			hingeSpring.targetPosition = 0;
			hingeJoint.spring = hingeSpring;

			hingeJoint.useSpring = true;
		}

		if (m_addLimits)
		{
			JointLimits limits = hingeJoint.limits;
			limits.min = -5;
			limits.max = 5;

			hingeJoint.limits = limits;
			hingeJoint.useLimits = true;
		}

		m_colliderToIgnore = m_connectedBody.GetComponent<Collider>();

		// make sure we're using gravity once it's been grabbed
		this.transform.GetComponent<Rigidbody>().useGravity = true;

		setCollidersToIgnore();
	}

	public void attachMeshes(Transform connectedBody, Vector3 hitPoint, Vector3 axis, GrabbableConfig config)
	{
		m_gripPoint = m_hitPoint = hitPoint;
		m_axis = axis;

		setupGrab(connectedBody, config);
	}

	private void setupGrab(Transform connectedBody, GrabbableConfig config)
	{
		// setup the grab here, then do it in the next frame (in Update()) to make sure everything's up to date
		m_connectedBody = connectedBody;
		m_breakForce = config.breakForce;
		m_breakTorque = config.breakTorque;
		m_addSpring = config.addSpring;
		m_addLimits = config.addLimits;
		m_degradeHingeOverTime = config.degradeHingeOverTime;

		if (config.addHinge)
		{
			CreateHinge();
		}
		else //  use this if we don't want the discuit to swing
		{
			this.transform.parent = connectedBody;

			// make sure we're not using gravity once it's been grabbed
			this.transform.GetComponent<Rigidbody>().useGravity = false;

			this.transform.GetComponent<Collider>().isTrigger = true;
		}

		Debug.Log( this.name + " has been grabbed" );
    }

    public void UpdateHingeAfterCut( Transform newConnectedBody, Transform cuttingPlane )
    {
		HingeJoint hingeJoint = this.GetComponent<HingeJoint>();
		if (hingeJoint != null)
		{
			Debug.Log("Trying to grab a cartridge that already has a hinge joint?!");
		}

		hingeJoint.connectedBody = newConnectedBody.GetComponent<Rigidbody>();

		float anchorY = cuttingPlane.InverseTransformPoint(cuttingPlane.position).y;

		// update the location so it's at the correct height
		Vector3 anchor = hingeJoint.anchor;
        anchor.y = anchorY;
		hingeJoint.anchor = anchor;

		hingeJoint.autoConfigureConnectedAnchor = false;

		Vector3 connectedAnchor = hingeJoint.connectedAnchor;
		connectedAnchor.y = cuttingPlane.position.y;
		hingeJoint.connectedAnchor = connectedAnchor;

		m_colliderToIgnore = newConnectedBody.GetComponent<Collider>();

		// make sure we're using gravity once it's been grabbed
		this.transform.GetComponent<Rigidbody>().useGravity = true;

		setCollidersToIgnore();
    }

	void OnJointBreak( float breakForce )
	{
		// don't have it flying off
		Rigidbody rigidbody = GetComponent<Rigidbody>();

		rigidbody.velocity /= 10.0f;
	}

	void OnDrawGizmos()
	{
		if (m_gripPoint != m_hitPoint)
		{
			Gizmos.color = Color.blue;
			Gizmos.DrawWireSphere(m_hitPoint, 0.25f);

			Gizmos.color = Color.red;
			Gizmos.DrawWireSphere(m_gripPoint, 0.25f);

			Gizmos.DrawLine(m_hitPoint, m_gripPoint);

			Gizmos.color = Color.green;
			Gizmos.DrawWireSphere(m_newPosition, 0.375f);

			Gizmos.DrawLine(this.transform.position, m_newPosition);

			Gizmos.color = Color.yellow;
			Gizmos.DrawWireSphere(this.transform.position, 0.25f);

			Gizmos.color = Color.white;
		}
	}
}
