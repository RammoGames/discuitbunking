﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Grip : MonoBehaviour 
{
    private List<Grabbable> m_lGrabbables = null;

	private Hand m_parentHand = null;

	// Use this for initialization
	void Start () 
    {
		// GetComponentInParent is called recursively upwards until it finds a parent with the right component
		m_parentHand = gameObject.GetComponentInParent<Hand>();
	}
	
	// Update is called once per frame
	void Update ()
    {
	
	}


	void OnTriggerEnter(Collider otherCollider)
	{
		Grabbable grabbable = otherCollider.GetComponent<Grabbable>();
		if (grabbable == null)
		{
			return;
		}

		if (m_lGrabbables == null)
		{
			m_lGrabbables = new List<Grabbable>();
		}

		m_lGrabbables.Add(grabbable);
		//Debug.Log("Grabbable added to list");

        if(Debug.isDebugBuild)
        {
			Debug.Log("Potential grabbable: " + grabbable.name);
		}

		m_parentHand.HandleGrabbableAdded();
	}

    void OnTriggerExit( Collider otherCollider )
    {
        Grabbable grabbable = otherCollider.GetComponent<Grabbable>();
        if ( grabbable == null )
        {
            return;
        }

        m_lGrabbables.Remove( grabbable );
        Debug.Log( "Grabbable removed from list" );
    }

    public Grabbable getGrabbableToGrab( ref Vector3 hitPoint, ref Vector3 axis )
    {
        if( m_lGrabbables == null )
        {
            return null;
        }

        Vector3 point = this.transform.position;
        float shortestGrabbableDistance = Mathf.Infinity;

        Grabbable closestGrabbable  = null;

        Grabbable grabbableInside = null;

        foreach ( Grabbable grabbable in m_lGrabbables )
        {
            if ( grabbable == null )
            {
                continue;
            }

            float distance = Mathf.Infinity;
            bool isInside = gripIsInsideGrabbable( grabbable.GetComponent<Collider>(), point, ref distance, ref hitPoint );
            if ( isInside )
            {
                grabbableInside = grabbable;
            }

            if ( distance < shortestGrabbableDistance )
            {
                shortestGrabbableDistance = distance;
                closestGrabbable = grabbable;
            }
        }

        if ( closestGrabbable == null )
        {
            Debug.Log( "Closest grabbable is too far away: " + shortestGrabbableDistance );
		}
		else if (grabbableInside != null) // then anything we're inside
		{
			closestGrabbable = grabbableInside;

			axis = this.transform.right;

			Debug.Log("Local right: " + this.transform.right.ToString());
			Debug.Log("Local right in world: " + axis.ToString());
		}

		m_lGrabbables.Clear(); // remove all remaining grabbables, just in case

		return closestGrabbable;
    }

    private bool gripIsInsideGrabbable( Collider test, Vector3 point, ref float distance, ref Vector3 hitPoint )
    {
        Vector3    centre;
        Vector3    direction;
        Ray        ray;
        RaycastHit hitInfo;
        bool       hit;

        // Use collider bounds to get the center of the collider. May be inaccurate
        // for some colliders (i.e. MeshCollider with a 'plane' mesh)
        centre = test.bounds.center;

        // Cast a ray from point to center
        direction = centre - point;
        ray = new Ray( point, direction );
        hit = test.Raycast( ray, out hitInfo, direction.magnitude );

        //if ( Debug.isDebugBuild )
        //{
        //    Debug.DrawRay( point, direction, Color.red, Mathf.Infinity, false );
        //}

        distance = hitInfo.distance;
        hitPoint = hitInfo.point;

        Debug.Log( "hitPoint: " + hitPoint.ToString() );
        Debug.Log( "Collider position: " + centre.ToString() );
        Debug.Log( "Grip position: " + point.ToString() );
        
        if ( !hit )
        {
            hitPoint = point;
            Debug.Log( "Inside collider" );
            return true; // we're inside the collider, how close do you want to be?
        }

        // If we hit the collider, point is outside. So we return !hit
        return false;
    }
}
