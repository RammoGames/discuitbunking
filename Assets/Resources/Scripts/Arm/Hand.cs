﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;

public class Hand : MonoBehaviour 
{
    public const string TOGGLEHOLD = "toggleHold";

	[SerializeField]
    private Grip m_grip = null;

	[SerializeField]
    private Grabbable m_currentGrabbable = null;

    private UnityAction m_toggleHold;

    [SerializeField]
	private Animator m_animator = null;

	private Transform m_handObject = null;

	// private Vector3 m_grabbableTargetPosition = Vector3.zero;
	// private Rigidbody m_grabbableRigidbody = null;

	// Use this for initializationW
	void Awake () 
    {
        m_toggleHold = new UnityAction( toggleHold );

		m_handObject = this.transform.Find("HandObject");
	}

    void Update()
    {
    }

    void OnEnable()
    {
        EventManager.startListening( TOGGLEHOLD, m_toggleHold );
    }

    void OnDisable()
    {
        EventManager.stopListening( TOGGLEHOLD, m_toggleHold );
    }

    public Grabbable currentGrabbable
    {
        get { return m_currentGrabbable; }
    }
	
	// Update is called once per frame
	// void FixedUpdate () 
    // {
    //     if( m_currentGrabbable )
    //     {
	// 		m_grabbableRigidbody.MovePosition(m_grabbableTargetPosition);
	// 	}
	// }

    void toggleHold()
    {
        if( m_currentGrabbable != null )
        {
            m_currentGrabbable.letGo();
            m_currentGrabbable = null;

			m_animator.SetTrigger("LetGo");
		}
        else
        {
			m_animator.SetTrigger("Grab");

            Vector3 hitPoint = Vector3.zero;
            Vector3 axis = Vector3.right;
            m_currentGrabbable = m_grip.getGrabbableToGrab( ref hitPoint, ref axis );
            if( m_currentGrabbable )
            {
				Grabbable.GrabbableConfig config;
				config.addHinge = true;
				config.breakForce = Mathf.Infinity;
				config.breakTorque = Mathf.Infinity;
				config.addSpring = false;
				config.addLimits = false;
				config.degradeHingeOverTime = false;

				StartCoroutine(m_currentGrabbable.GrabWithHand(m_grip.transform, m_grip.transform.position, this.transform.right, config));
            }
            else
            {
				m_animator.SetTrigger("LetGo");
            }
        }
    }

    public void HandleGrabbableAdded()
    {
		m_animator.SetTrigger("Idle");
	}    
}
