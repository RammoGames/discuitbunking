﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;

public class ArmControl : MonoBehaviour
{
    // public
    // Action identifiers
    public const string MOVEUPANDDOWN           = "MoveUpAndDown";
    public const string MOVELEFTANDRIGHT        = "MoveLeftAndRight";
    public const string CLEARMOVEUPANDDOWN      = "ClearMoveUpAndDown";
    public const string CLEARMOVELEFTANDRIGHT   = "ClearMoveLeftAndRight";

    [SerializeField]
    private Hand m_hand = null;

	// private
	// constant values
	[SerializeField]
    private float MIN_X_DISTANCE = -5.0f, MAX_X_DISTANCE = 5.0f;

	[SerializeField]
    private float MIN_Z_DISTANCE = -6.0f, MAX_Z_DISTANCE = 4.0f;

	[SerializeField]
    private float MIN_Y_DISTANCE = 0.0f, MAX_Y_DISTANCE = 5.0f;

    // variables
    private bool m_bIsMovingUpAndDown           = false;
    private bool m_bIsMovingLeftAndRight        = false;

    // Actions
    private UnityAction m_listenerMoveUpAndDown;
    private UnityAction m_listenerMoveLeftAndRight;

    private UnityAction m_listenerClearMoveUpAndDown;
    private UnityAction m_listenerClearMoveLeftAndRight;

    // Use this for initialization
    void Awake()
    {
        m_listenerMoveUpAndDown         = new UnityAction( setMoveUpAndDown );
        m_listenerMoveLeftAndRight      = new UnityAction( setMoveLeftAndRight );

        m_listenerClearMoveUpAndDown    = new UnityAction( clearMoveUpAndDown );
        m_listenerClearMoveLeftAndRight = new UnityAction( clearMoveLeftAndRight );
    }

    private void setMoveUpAndDown()
    {
        m_bIsMovingUpAndDown        = true;
    }

    private void setMoveLeftAndRight()
    {
        m_bIsMovingLeftAndRight     = true;
    }

    private void clearMoveUpAndDown()
    {
        m_bIsMovingUpAndDown        = false;
    }

    private void clearMoveLeftAndRight()
    {
        m_bIsMovingLeftAndRight     = false;
    }

    void OnEnable()
    {
        if ( m_listenerMoveUpAndDown != null )
        {
            EventManager.startListening( MOVEUPANDDOWN, m_listenerMoveUpAndDown );
        }

        if ( m_listenerMoveLeftAndRight != null )
        {
            EventManager.startListening( MOVELEFTANDRIGHT, m_listenerMoveLeftAndRight );
        }

        if ( m_listenerClearMoveUpAndDown != null )
        {
            EventManager.startListening( CLEARMOVEUPANDDOWN, m_listenerClearMoveUpAndDown );
        }

        if ( m_listenerClearMoveLeftAndRight != null )
        {
            EventManager.startListening( CLEARMOVELEFTANDRIGHT, m_listenerClearMoveLeftAndRight );
        }
    }

    void OnDisable()
    {
        EventManager.stopListening( MOVEUPANDDOWN, m_listenerMoveUpAndDown );
        EventManager.stopListening( CLEARMOVEUPANDDOWN, m_listenerClearMoveUpAndDown );

        EventManager.stopListening( MOVEUPANDDOWN, m_listenerMoveUpAndDown );
        EventManager.stopListening( CLEARMOVELEFTANDRIGHT, m_listenerClearMoveLeftAndRight );
    }

    // Update is called once per frame
    void Update()
    {      
        float X = InputManager.X;
		float Y = InputManager.Y;

        if ( m_hand.currentGrabbable )
        {
            float drag = m_hand.currentGrabbable.GetComponent<Rigidbody>().drag;
            if ( drag != 0 )
            {
                float dragModifier = 1 / drag;
                X *= dragModifier;
                Y *= dragModifier;
            }
        }

        //Debug.Log( mouseDelta );

        Vector3 newPosition = this.transform.position;
        if ( !m_bIsMovingUpAndDown )
        {
            // limit the z value to the defined constants
            float mouseDeltaZ = Mathf.Max( Mathf.Min( this.transform.position.z + Y, MAX_Z_DISTANCE ), MIN_Z_DISTANCE );
            
            // work out what the new position should be
            newPosition.z = mouseDeltaZ;
            //Debug.Log( "Mouse button up" );
        }
        else
        {
            float mouseDeltaY = Mathf.Max( Mathf.Min( this.transform.position.y + Y, MAX_Y_DISTANCE ), MIN_Y_DISTANCE );

            // work out what the new position should be
            newPosition.y = mouseDeltaY;
            //Debug.Log( "Mouse button down" );
        }

        if( !m_bIsMovingLeftAndRight )
        {
            // set the rotation based upon the left and right motion of the mouse
            
            Vector3 rotation = this.transform.eulerAngles;

            rotation.y += X;

            if ( rotation.y > 270.0f || rotation.y < 90.0f )
            {
                this.transform.eulerAngles = new UnityEngine.Vector3( rotation.x, rotation.y, rotation.z );
            }

            //Debug.Log( "Angle: " + this.transform.rotation.eulerAngles );
            
        }
        else
        {
            float mouseDeltaX = Mathf.Max( Mathf.Min( this.transform.position.x + ( X / 2.0f ), MAX_X_DISTANCE ), MIN_X_DISTANCE );

            newPosition.x = mouseDeltaX;
        }

        //Debug.Log( "Old position: " + this.transform.position );

        // set the new position and smooth it out a bit
        //float step = Time.time / TIMEDIVIDER;
        this.transform.position = Vector3.Lerp( this.transform.position, newPosition, 0.5f );
        //Debug.Log( step );

        //Debug.Log( "New position: " + this.transform.position );
        // 
        
        // store this mouse position for next time
        //m_vLastMousePosition = currentMousePosition;
    }
}
