﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour 
{
    public Transform m_objectToFollow;

    private Vector3 m_vOffset;
    private Quaternion m_rotation;

    [SerializeField]
    public float m_fSmooth = 1.5f;

    [SerializeField]
    private float MAX_X = 2.5f, MIN_X = -2.5f;

	[SerializeField]
	private Transform m_cameraLimitPlane = null;

	// Use this for initialization
	void Start () 
    {
        if( m_objectToFollow == null )
        {
            Debug.LogError( "Set the camera's object to follow" );
            Debug.Break();
        }

        m_vOffset = this.transform.position - m_objectToFollow.position;
        m_rotation = this.transform.rotation;
	}
	
	// Update is called once per frame
	void Update () 
    {
        Vector3 newPosition = m_objectToFollow.transform.position + m_vOffset;
        Quaternion newRotation = m_rotation;
        if( newPosition.x > MAX_X )
        {
            newPosition.x = MAX_X;           
        }
        else if( newPosition.x < MIN_X )
        {
            newPosition.x = MIN_X;
        }

        //newPosition.y -= DIP_Y * Mathf.Clamp( Mathf.Abs( newPosition.x / MAX_X ), 0, 1 );

        if( newPosition.z > m_cameraLimitPlane.position.z )
        {
			newPosition.z = m_cameraLimitPlane.position.z;
		}

        if( newPosition.y < 2.0f )
        {
			newPosition.y = 2.0f;
		}

		newRotation = Quaternion.Euler( m_rotation.eulerAngles.x, this.transform.rotation.eulerAngles.y, m_rotation.eulerAngles.z );


        this.transform.position = Vector3.Lerp( this.transform.position, newPosition, m_fSmooth * Time.deltaTime );
        //this.transform.rotation = Quaternion.Lerp( this.transform.rotation, newRotation, m_fSmooth * Time.deltaTime );
	}
}
