﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;

public class CameraMove : MonoBehaviour
{
    private Vector3 m_vTargetPosition = new Vector3( -2.45f, 5.91f, -24f );
    private const float m_fSmooth = 1.5f;

	// Use this for initialization
	void Start () 
    {
	
	}
	
	// Update is called once per frame
	void Update () 
    {
        this.transform.position = Vector3.Lerp( this.transform.position, m_vTargetPosition, m_fSmooth * Time.deltaTime );
        this.transform.rotation = Quaternion.Lerp( this.transform.rotation, Quaternion.identity, m_fSmooth * Time.deltaTime );
	}
}
