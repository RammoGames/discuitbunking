﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class RippleCamera : MonoBehaviour
{
	private Vector3 m_position = Vector3.positiveInfinity;

	private Camera m_camera = null;

	public Vector3 Position
    {
		set { m_position = value; }
	}

	void Awake()
    {
		// send some data to the shader
		m_camera = GetComponent<Camera>();

		Shader.SetGlobalTexture("_RippleEffectRT", m_camera.targetTexture);
		Shader.SetGlobalFloat("_CameraSize", m_camera.orthographicSize);
	}

    // Update is called once per frame
    void Update()
    {
		// if (m_position.x != float.PositiveInfinity)
		{
			//this.transform.position = m_position;
			Shader.SetGlobalVector("_Position", this.transform.position);
		}
	}

    void OnEnable()
    {
		m_camera.enabled = true;
	}

    void OnDisable()
    {
		m_camera.enabled = false;
		m_camera.targetTexture.DiscardContents();
	}
}
