﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cookie : MonoBehaviour
{
	[SerializeField]
	private ParticleSystem m_crumbs = null;

	[SerializeField]
	private float m_minVelocity = 10.0f;

	void OnCollisionEnter( Collision collision )
    {
		//Debug.Log("Collision velocity: " + collision.relativeVelocity.magnitude);
		if (collision.relativeVelocity.magnitude > m_minVelocity)
		{
			for (int i = 0; i < collision.contactCount; ++i)
			{
				ParticleSystem crumbs = Instantiate(m_crumbs);
				crumbs.transform.position = collision.GetContact(i).point;
				crumbs.transform.LookAt(this.transform);
				crumbs.transform.rotation = Quaternion.Inverse(crumbs.transform.rotation);

				crumbs.Play();

				Destroy(crumbs, crumbs.main.startLifetime.constantMax);
			}
		}
	}
}
