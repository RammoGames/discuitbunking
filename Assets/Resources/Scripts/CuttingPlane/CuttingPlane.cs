﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CuttingPlane : MonoBehaviour
{
	[SerializeField]
	private LayerMask m_cuttingLayer = 0;

	[SerializeField]
	private ParticleSystem m_rippleParticles = null;

	private float m_cuttingTime = 0.0f;
	private bool m_allowCutting = true;
	private float m_allowCuttingTimer = 0.0f;
	private float m_cuttingPause = 2.0f;

	[SerializeField] // show this in the inspector for debugging
	private List<Ripples> m_ripples = new List<Ripples>();

	[System.Serializable]
	private struct Ripples
	{
		public Transform m_transform;
		public ParticleSystem m_particleSystem;
	}

	// Start is called before the first frame update
	void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if( !m_allowCutting )
        {
			m_allowCuttingTimer += Time.deltaTime;
            if( m_allowCuttingTimer > m_cuttingPause )
            {
				m_allowCutting = true;
				m_allowCuttingTimer = 0.0f;
			}
		}

		for (int i = m_ripples.Count; i-- > 0;)
		{
			ParticleSystem ripples = m_ripples[i].m_particleSystem;
			if (ripples.particleCount == 0 || m_ripples[i].m_transform == null)
			{
				// no particlaes left, so destroy the particle system
				Destroy(ripples.gameObject);
				m_ripples.Remove(m_ripples[i]);
			}
		}
	}

	private void AddNewRipple( Transform transform )
	{
		Ripples newRipples = new Ripples();
		newRipples.m_transform = transform;
		newRipples.m_particleSystem = Instantiate(m_rippleParticles, transform.position, transform.rotation, this.transform);

		m_ripples.Add(newRipples);
	}

	void OnTriggerEnter( Collider collider )
	{
		if (m_rippleParticles)
		{
			Ripples ripples = m_ripples.Find(x => x.m_transform == collider.transform);
			if (!ripples.m_transform)
			{
				AddNewRipple(collider.transform);
			}
		}
	}

    void OnTriggerStay( Collider collider )
    {
		Ripples ripple = m_ripples.Find(x => x.m_transform == collider.transform);
		if (ripple.m_transform)
		{
			Vector3 newPosition = collider.transform.position;
			newPosition.y = this.transform.position.y; // snap the particles to the plane
			ripple.m_particleSystem.transform.position = newPosition;
		}
		else
		{
			AddNewRipple(collider.transform);
		}

		if( m_allowCutting && 1 << collider.gameObject.layer == m_cuttingLayer )
        {
			m_cuttingTime += Time.deltaTime;

			Cuttable cuttable = collider.GetComponent<Cuttable>();
			if( cuttable && m_cuttingTime > cuttable.cutTime )
            {
				GameObject[] cutObjects = Cutter.Cut(collider.gameObject, this.transform.position, this.transform.up, cuttable.cutMaterial, true, true);
				m_cuttingTime = 0.0f;
				m_allowCutting = false;

				// get the old object and make sure it can't collide
				cutObjects[0].GetComponent<Collider>().isTrigger = true;

				// get the new object and make sure it can collide again
				cutObjects[1].GetComponent<Collider>().isTrigger = false;

				Cuttable topCuttable = cutObjects[0].GetComponent<Cuttable>();
				Cuttable bottomCuttable = cutObjects[1].GetComponent<Cuttable>();

				// find the first grabbable in an existing list
				CuttableList list = CuttableListList.getInstance().FindCuttableList(topCuttable);
				if( list == null )
				{
					list = new CuttableList();
					list.Add(topCuttable);
				}

				// set the y value to that of the cutting plane, then we have the correct hinge location
				float cutYPosition = this.transform.position.y;
				
				list.InsertCuttable(topCuttable, bottomCuttable, cutYPosition);

				MeshRenderer meshRenderer = bottomCuttable.GetComponent<MeshRenderer>();

				List<Material> materials = new List<Material>();
				meshRenderer.GetMaterials(materials);

				// darken what's been in the drink
				foreach( Material material in materials)
				{
					material.color = new Color(material.color.r * 0.5f, material.color.g * 0.5f, material.color.b * 0.5f, material.color.a);
				}

				Renderer renderer1 = cutObjects[0].GetComponent<Renderer>();
				Renderer renderer2 = cutObjects[1].GetComponent<Renderer>();
			}
		}
    }

	void OnTriggerExit(Collider collider)
	{
		Ripples ripple = m_ripples.Find(x => x.m_transform == collider.transform);
		ripple.m_particleSystem.Stop(); // stop creating more particles but allow the existing particles to finish
	}
}
