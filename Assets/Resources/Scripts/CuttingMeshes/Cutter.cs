﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class GeneratedMesh
{
	List<Vector3> m_vertices = new List<Vector3>();
	List<Vector3> m_normals = new List<Vector3>();

	List<Vector2> m_uvs = new List<Vector2>();
	List<List<int>> m_submeshIndices = new List<List<int>>();

	public List<Vector3> vertices
	{
		get { return m_vertices; }
		set { m_vertices = value; }
	}

	public List<Vector3> normals
	{
		get { return m_normals; }
		set { m_normals = value; }
	}

	public List<Vector2> uvs
	{
		get { return m_uvs; }
		set { m_uvs = value; }
	}

	public List<List<int>> submeshIndices
	{
		get { return m_submeshIndices; }
		set { m_submeshIndices = value; }
	}

	public void AddTriangle(MeshTriangle triangle)
	{
		int currentVertexCount = m_vertices.Count;

		m_vertices.AddRange(triangle.vertices);
		m_normals.AddRange(triangle.normals);
		m_uvs.AddRange(triangle.uvs);

		// make sure we have the right number of submesh indices
		if (m_submeshIndices.Count < triangle.submeshIndex + 1)
		{
			for (int i = m_submeshIndices.Count; i < triangle.submeshIndex + 1; ++i)
			{
				m_submeshIndices.Add(new List<int>());
			}
		}

		for (int i = 0; i < 3; ++i)
		{
			m_submeshIndices[triangle.submeshIndex].Add(currentVertexCount + i);
		}
	}
}

public class MeshTriangle
{
	List<Vector3> m_vertices = new List<Vector3>();
	List<Vector3> m_normals = new List<Vector3>();

	List<Vector2> m_uvs = new List<Vector2>();
	int m_submeshIndex; //<! submesh index that the triangle belongs to

	public List<Vector3> vertices
	{
		get { return m_vertices; }
		set { m_vertices = value; }
	}

	public List<Vector3> normals
	{
		get { return m_normals; }
		set { m_normals = value; }
	}

	public List<Vector2> uvs
	{
		get { return m_uvs; }
		set { m_uvs = value; }
	}

	public int submeshIndex
	{
		get { return m_submeshIndex; }
		set { m_submeshIndex = value; }
	}

	public MeshTriangle(Vector3[] vertices, Vector3[] normals, Vector2[] uvs, int submeshIndex)
	{
		Clear();

		m_vertices.AddRange(vertices);
		m_normals.AddRange(normals);
		m_uvs.AddRange(uvs);

		m_submeshIndex = submeshIndex;
	}

	public void Clear()
	{
		m_vertices.Clear();
		m_normals.Clear();
		m_uvs.Clear();

		m_submeshIndex = 0;
	}
}

public class Cutter
{
	public static bool m_currentlyCutting = false;
	public static Mesh m_originalMesh;

	public static GameObject[] Cut(GameObject originalGameObject, Vector3 contactPoint, Vector3 cutNormal, Material cutMaterial = null, bool fill = true, bool addRigidbody = false)
	{
		if (m_currentlyCutting)
		{
			return null;
		}

		m_currentlyCutting = true;

		Plane plane = new Plane(originalGameObject.transform.InverseTransformDirection(cutNormal), // Transforms a direction from world space to local space - used here as the normal
								originalGameObject.transform.InverseTransformPoint(contactPoint)); // Transforms a point from world space to local space

		m_originalMesh = originalGameObject.GetComponent<MeshFilter>().mesh;

		List<Vector3> addedVertices = new List<Vector3>();

		GeneratedMesh leftMesh = new GeneratedMesh();
		GeneratedMesh rightMesh = new GeneratedMesh();

		int[] submeshIndices;
		int triangleIndexA, triangleIndexB, triangleIndexC;

		// go through the submeshes on the original mesh so that we can account for multiple materials
		for (int i = 0; i < m_originalMesh.subMeshCount; ++i)
		{
			submeshIndices = m_originalMesh.GetTriangles(i);

			for (int j = 0; j < submeshIndices.Length; j += 3)
			{
				triangleIndexA = submeshIndices[j];
				triangleIndexB = submeshIndices[j + 1];
				triangleIndexC = submeshIndices[j + 2];

				MeshTriangle currentTriangle = GetTriangle(triangleIndexA, triangleIndexB, triangleIndexC, i);

				bool triangleALeftSide = plane.GetSide(m_originalMesh.vertices[triangleIndexA]);
				bool triangleBLeftSide = plane.GetSide(m_originalMesh.vertices[triangleIndexB]);
				bool triangleCLeftSide = plane.GetSide(m_originalMesh.vertices[triangleIndexC]);

				if (triangleALeftSide && triangleBLeftSide && triangleCLeftSide)
				{
					// all vertices are on the left
					leftMesh.AddTriangle(currentTriangle);
				}
				else if (!triangleALeftSide && !triangleBLeftSide && !triangleCLeftSide)
				{
					// all vertices are on the right
					rightMesh.AddTriangle(currentTriangle);
				}
				else
				{
					CutTriangle(plane, currentTriangle, triangleALeftSide, triangleBLeftSide, triangleCLeftSide, ref leftMesh, ref rightMesh, ref addedVertices);
				}
			}
		}

		FillCut(addedVertices, plane, ref leftMesh, ref rightMesh);

		// make a new gameobject to hold half of the old object
		Transform originalParent = originalGameObject.transform.parent;
		originalGameObject.transform.parent = null;
		GameObject newGameObject = GameObject.Instantiate(originalGameObject);
		originalGameObject.transform.parent = originalParent;

		// remove any existing colliders
		Collider[] originalColliders = originalGameObject.GetComponentsInChildren<Collider>();
		Collider[] newColliders = newGameObject.GetComponentsInChildren<Collider>();

		// because we have to use Destroy instead of DestroyImmediate (can't use DestroyImmediate during collisions) we have to clear out the colliders on both objects
		// because they won't be destroyed until later in the frame, which is too late for the new object
		for (int i = 0; i < originalColliders.Length; ++i)
		{
			// remove the collider
			GameObject.Destroy(originalColliders[i]);
			GameObject.Destroy(newColliders[i]);
		}

		void AddMesh(GeneratedMesh mesh, ref GameObject gameObject, string name)
		{
			// create a new mesh on the original gameobject
			Mesh newMesh = new Mesh();
			newMesh.Clear();
			newMesh.SetVertices(mesh.vertices);

			// set the submesh count so that we can setup the triangles
			newMesh.subMeshCount = mesh.submeshIndices.Count;
			for (int i = 0; i < mesh.submeshIndices.Count; ++i)
			{
				newMesh.SetTriangles(mesh.submeshIndices[i], i);
			}

			List<Material> materials = new List<Material>();
			materials.AddRange(gameObject.GetComponent<Renderer>().sharedMaterials);
			materials.Add(cutMaterial); // we've created a new submesh so add the cutMaterial for it

			gameObject.GetComponent<Renderer>().sharedMaterials = materials.ToArray();

			// set everything else
			newMesh.SetNormals(mesh.normals);
			newMesh.SetUVs(0, mesh.uvs);
			newMesh.RecalculateBounds();
			NormalSolver.RecalculateNormals(newMesh, 30);
			newMesh.Optimize();
			newMesh.name = name;

			MeshCollider meshCollider = gameObject.AddComponent<MeshCollider>();
			meshCollider.sharedMesh = newMesh;
			meshCollider.convex = true;

			gameObject.GetComponent<MeshFilter>().sharedMesh = newMesh;
		}

		// create a new mesh on the original gameobject
		AddMesh(leftMesh, ref originalGameObject, "OriginalMesh");
		AddMesh(rightMesh, ref newGameObject, "NewMesh");

		if (addRigidbody)
		{
			Rigidbody dummyRigidbody = null;
			float originalMinY = originalGameObject.GetComponent<Renderer>().bounds.min.y;
			float newMinY = newGameObject.GetComponent<Renderer>().bounds.min.y;
			if (originalMinY < newMinY)
			{
				if (!originalGameObject.transform.TryGetComponent<Rigidbody>(out dummyRigidbody))
				{
					dummyRigidbody = originalGameObject.AddComponent<Rigidbody>();
				}
			}
			else
			{
				if (!newGameObject.transform.TryGetComponent<Rigidbody>(out dummyRigidbody))
				{
					dummyRigidbody = newGameObject.AddComponent<Rigidbody>();
				}
			}

			dummyRigidbody.useGravity = true;
			dummyRigidbody.isKinematic = false;
		}

		m_currentlyCutting = false;

		GameObject[] gameObjects = new GameObject[2];
		gameObjects[0] = originalGameObject;
		gameObjects[1] = newGameObject;
		return gameObjects;
	}

	private static MeshTriangle GetTriangle(int indexA, int indexB, int indexC, int submeshIndex)
	{
		Vector3[] vertices = { m_originalMesh.vertices[indexA], m_originalMesh.vertices[indexB], m_originalMesh.vertices[indexC] };
		Vector3[] normals = { m_originalMesh.normals[indexA], m_originalMesh.normals[indexB], m_originalMesh.normals[indexC] };
		Vector2[] uvs = { m_originalMesh.uv[indexA], m_originalMesh.uv[indexB], m_originalMesh.uv[indexC] };

		return new MeshTriangle(vertices, normals, uvs, submeshIndex);
	}

	private static void CutTriangle(Plane plane, MeshTriangle triangle, bool triangleALeftSide, bool triangleBLeftSide, bool triangleCLeftSide, ref GeneratedMesh leftMesh, ref GeneratedMesh rightMesh, ref List<Vector3> addedVertices)
	{
		List<bool> leftSide = new List<bool>();
		leftSide.Add(triangleALeftSide);
		leftSide.Add(triangleBLeftSide);
		leftSide.Add(triangleCLeftSide);

		MeshTriangle leftMeshTriangle = new MeshTriangle(new Vector3[2], new Vector3[2], new Vector2[2], triangle.submeshIndex);
		MeshTriangle rightMeshTriangle = new MeshTriangle(new Vector3[2], new Vector3[2], new Vector2[2], triangle.submeshIndex);

		bool left = false, right = false;

		// grab the vertices and keep track of what side they should be on
		for (int i = 0; i < 3; ++i)
		{
			if (leftSide[i]) // one of the triangle sides passed in is true
			{
				if (!left) // either we were right, or neither
				{
					left = true;

					leftMeshTriangle.vertices[0] = triangle.vertices[i];
					leftMeshTriangle.vertices[1] = leftMeshTriangle.vertices[0];

					leftMeshTriangle.normals[0] = triangle.normals[i];
					leftMeshTriangle.normals[1] = leftMeshTriangle.normals[0];

					leftMeshTriangle.uvs[0] = triangle.uvs[i];
					leftMeshTriangle.uvs[1] = leftMeshTriangle.uvs[0];
				}
				else
				{
					leftMeshTriangle.vertices[1] = triangle.vertices[i];
					leftMeshTriangle.normals[1] = triangle.normals[i];
					leftMeshTriangle.uvs[1] = triangle.uvs[i];
				}
			}
			else
			{
				if (!right)
				{
					right = true;

					rightMeshTriangle.vertices[0] = triangle.vertices[i];
					rightMeshTriangle.vertices[1] = rightMeshTriangle.vertices[0];

					rightMeshTriangle.normals[0] = triangle.normals[i];
					rightMeshTriangle.normals[1] = rightMeshTriangle.normals[0];

					rightMeshTriangle.uvs[0] = triangle.uvs[i];
					rightMeshTriangle.uvs[1] = rightMeshTriangle.uvs[0];
				}
				else
				{
					rightMeshTriangle.vertices[1] = triangle.vertices[i];
					rightMeshTriangle.normals[1] = triangle.normals[i];
					rightMeshTriangle.uvs[1] = triangle.uvs[i];
				}
			}
		}

		float normalisedDistance;
		float distance;

		// do the left
		plane.Raycast(new Ray(leftMeshTriangle.vertices[0], (rightMeshTriangle.vertices[0] - leftMeshTriangle.vertices[0]).normalized), out distance);

		normalisedDistance = distance / (rightMeshTriangle.vertices[0] - leftMeshTriangle.vertices[0]).magnitude;

		Vector3 vertexLeft = Vector3.Lerp(leftMeshTriangle.vertices[0], rightMeshTriangle.vertices[0], normalisedDistance);
		Vector3 normalLeft = Vector3.Lerp(leftMeshTriangle.normals[0], rightMeshTriangle.normals[0], normalisedDistance);
		Vector3 uvsLeft = Vector3.Lerp(leftMeshTriangle.uvs[0], rightMeshTriangle.uvs[0], normalisedDistance);

		addedVertices.Add(vertexLeft);

		// do the right
		plane.Raycast(new Ray(leftMeshTriangle.vertices[1], (rightMeshTriangle.vertices[1] - leftMeshTriangle.vertices[1]).normalized), out distance);

		normalisedDistance = distance / (rightMeshTriangle.vertices[1] - leftMeshTriangle.vertices[1]).magnitude;

		Vector3 vertexRight = Vector3.Lerp(leftMeshTriangle.vertices[1], rightMeshTriangle.vertices[1], normalisedDistance);
		Vector3 normalRight = Vector3.Lerp(leftMeshTriangle.normals[1], rightMeshTriangle.normals[1], normalisedDistance);
		Vector3 uvsRight = Vector3.Lerp(leftMeshTriangle.uvs[1], rightMeshTriangle.uvs[1], normalisedDistance);

		addedVertices.Add(vertexRight);

		// now create new MeshTriangles from the data above

		void addTriangles(MeshTriangle meshTriangle, ref GeneratedMesh mesh)
		{
			MeshTriangle currentTriangle;
			Vector3[] updatedVertices = new Vector3[] { meshTriangle.vertices[0], vertexLeft, vertexRight };
			Vector3[] updatedNormals = new Vector3[] { meshTriangle.normals[0], normalLeft, normalRight };
			Vector2[] updatedUVs = new Vector2[] { meshTriangle.uvs[0], uvsLeft, uvsRight };

			currentTriangle = new MeshTriangle(updatedVertices, updatedNormals, updatedUVs, triangle.submeshIndex);

			if (updatedVertices[0] != updatedVertices[1] && updatedVertices[0] != updatedVertices[2])
			{
				if (Vector3.Dot(Vector3.Cross(updatedVertices[1] - updatedVertices[0], updatedVertices[2] - updatedVertices[0]), updatedNormals[0]) < 0)
				{
					// we need to flip the normals
					FlipTriangle(ref currentTriangle);
				}

				mesh.AddTriangle(currentTriangle);
			}

			updatedVertices = new Vector3[] { meshTriangle.vertices[0], meshTriangle.vertices[1], vertexRight };
			updatedNormals = new Vector3[] { meshTriangle.normals[0], meshTriangle.normals[1], normalRight };
			updatedUVs = new Vector2[] { meshTriangle.uvs[0], meshTriangle.uvs[1], uvsRight };

			currentTriangle = new MeshTriangle(updatedVertices, updatedNormals, updatedUVs, triangle.submeshIndex);

			if (updatedVertices[0] != updatedVertices[1] && updatedVertices[0] != updatedVertices[2])
			{
				if (Vector3.Dot(Vector3.Cross(updatedVertices[1] - updatedVertices[0], updatedVertices[2] - updatedVertices[0]), updatedNormals[0]) < 0)
				{
					// we need to flip the normals
					FlipTriangle(ref currentTriangle);
				}

				mesh.AddTriangle(currentTriangle);
			}
		}

		addTriangles(leftMeshTriangle, ref leftMesh);
		addTriangles(rightMeshTriangle, ref rightMesh);
	}

	private static void FlipTriangle(ref MeshTriangle currentTriangle)
	{
		Vector3 lastVertex = currentTriangle.vertices[currentTriangle.vertices.Count - 1];
		currentTriangle.vertices[currentTriangle.vertices.Count - 1] = currentTriangle.vertices[0];
		currentTriangle.vertices[0] = lastVertex;

		Vector3 lastNormal = currentTriangle.normals[currentTriangle.normals.Count - 1];
		currentTriangle.normals[currentTriangle.normals.Count - 1] = lastNormal;
		currentTriangle.normals[0] = lastNormal;

		// for (int i = 0; i < currentTriangle.normals.Count; ++i )
		// {
		// 	currentTriangle.normals[i] *= -1.0f;
		// }

		Vector2 lastUV = currentTriangle.uvs[currentTriangle.uvs.Count - 1];
		currentTriangle.uvs[currentTriangle.uvs.Count - 1] = currentTriangle.uvs[0];
		currentTriangle.uvs[0] = lastUV;

		// currentTriangle.vertices.Reverse();
		// currentTriangle.normals.Reverse();
		// currentTriangle.uvs.Reverse();
	}

	public static void FillCut(List<Vector3> addedVertices, Plane plane, ref GeneratedMesh leftMesh, ref GeneratedMesh rightMesh)
	{
		List<Vector3> vertices = new List<Vector3>();
		List<Vector3> polygon = new List<Vector3>();

		for (int i = 0; i < addedVertices.Count; ++i)
		{
			if (!vertices.Contains(addedVertices[i]))
			{
				polygon.Clear();
				polygon.Add(addedVertices[i]);
				polygon.Add(addedVertices[i + 1]);

				vertices.Add(addedVertices[i]);
				vertices.Add(addedVertices[i + 1]);

				EvaluatePairs(addedVertices, ref vertices, ref polygon);
				Fill(polygon, plane, ref leftMesh, ref rightMesh);
			}
		}
	}

	private static void EvaluatePairs(List<Vector3> addedVertices, ref List<Vector3> vertices, ref List<Vector3> polygon)
	{
		bool isDone = false;
		while (!isDone)
		{
			isDone = true;
			for (int i = 0; i < addedVertices.Count; i += 2)
			{
				if (addedVertices[i] == polygon[polygon.Count - 1] && !vertices.Contains(addedVertices[i + 1]))
				{
					isDone = false;
					polygon.Add(addedVertices[i + 1]);
					vertices.Add(addedVertices[i + 1]);
				}
				else if (addedVertices[i + 1] == polygon[polygon.Count - 1] && !vertices.Contains(addedVertices[i]))
				{
					isDone = false;
					polygon.Add(addedVertices[i]);
					vertices.Add(addedVertices[i]);
				}
			}
		}
	}

	private static void Fill(List<Vector3> verticesIn, Plane plane, ref GeneratedMesh leftMesh, ref GeneratedMesh rightMesh)
	{
		// we need the centre so add up the vertices and calculate the average position
		Vector3 centrePosition = Vector3.zero;
		for (int i = 0; i < verticesIn.Count; ++i)
		{
			centrePosition += verticesIn[i];
		}

		centrePosition /= verticesIn.Count;

		// now get the normal of the plane for the upward axis
		Vector3 up = new Vector3(plane.normal.x, plane.normal.y, plane.normal.z);

		Vector3 left = Vector3.Cross(plane.normal, plane.normal);

		Vector3 displacement = Vector3.zero;
		Vector2 uv1 = Vector2.zero;
		Vector2 uv2 = Vector2.zero;

		for (int i = 0; i < verticesIn.Count; ++i)
		{
			displacement = verticesIn[i] - centrePosition;
			uv1 = new Vector2(0.5f + Vector3.Dot(displacement, left), 0.5f + Vector3.Dot(displacement, up));

			displacement = verticesIn[(i + 1) % verticesIn.Count] - centrePosition;
			uv2 = new Vector2(0.5f + Vector3.Dot(displacement, left), 0.5f + Vector3.Dot(displacement, up));

			Vector3[] _vertices = new Vector3[] { verticesIn[i], verticesIn[(i + 1) % verticesIn.Count], centrePosition };
			Vector3[] _normals = new Vector3[] { -plane.normal, -plane.normal, -plane.normal };
			Vector2[] _uvs = new Vector2[] { uv1, uv2, new Vector2(0.5f, 0.5f) };

			MeshTriangle currentTriangle = new MeshTriangle(_vertices, _normals, _uvs, m_originalMesh.subMeshCount);

			if (Vector3.Dot(Vector3.Cross(_vertices[1] - _vertices[0], _vertices[2] - _vertices[0]), _normals[0]) < 0)
			{
				FlipTriangle(ref currentTriangle);
			}
			leftMesh.AddTriangle(currentTriangle);

			_normals = new Vector3[] { plane.normal, plane.normal, plane.normal };

			currentTriangle = new MeshTriangle(_vertices, _normals, _uvs, m_originalMesh.subMeshCount);

			if (Vector3.Dot(Vector3.Cross(_vertices[1] - _vertices[0], _vertices[2] - _vertices[0]), _normals[0]) < 0)
			{
				FlipTriangle(ref currentTriangle);
			}
			rightMesh.AddTriangle(currentTriangle);
		}
	}
}