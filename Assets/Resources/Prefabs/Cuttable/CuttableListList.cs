﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CuttableListList : Singleton<CuttableListList>
{
	public List<CuttableList> m_grabbableLists = new List<CuttableList>();

    public CuttableList FindCuttableList( Cuttable grabbableToFind )
    {
        foreach( CuttableList list in m_grabbableLists )
        {
            if( list.Find( x => x == grabbableToFind ) )
            {
				return list;
			}
        }

		return null;
	}
}
