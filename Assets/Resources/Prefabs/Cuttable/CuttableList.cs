﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CuttableList : List<Cuttable>
{
    public void InsertCuttable( Cuttable parentCuttable, Cuttable newCuttable, float cutYposition )
    {
        if( Count == 1 )
        {
            // we've only got one item, so this one goes at the end
			Add(newCuttable);

			AddHinge(parentCuttable, newCuttable, cutYposition);

			return;
		}

		for (int i = 0; i < Count; ++i )
		{
			if (this[i] == parentCuttable)
			{
				if (i == Count - 1)
				{
					// we're at the end
					Add(newCuttable);

					AddHinge(parentCuttable, newCuttable, cutYposition);

					return;
				}
				else
				{
                    // squeeze the new grabbable inbetween the parent and the old grabbable
					Insert(i + 1, newCuttable);

					Cuttable nextGrabbable = this[i + 1];
					HingeJoint existingHingeJoint = nextGrabbable.GetComponent<HingeJoint>();

                    // convert the existing anchor back into world coordinates (grab takes world coordinate which it converts to local)
					Vector3 existingAnchor = nextGrabbable.transform.TransformPoint( existingHingeJoint.anchor );
					float breakForce = existingHingeJoint.breakForce;
					float breakTorque = existingHingeJoint.breakTorque;

                    // remove the old hinge
					GameObject.Destroy(existingHingeJoint);

                    // add a hinge between the parent and the new Grabbable
					AddHinge(parentCuttable, newCuttable, cutYposition);

					// add a hinge between the new Grabbable and the old grabbable using the old hinge settings
					AddHinge(newCuttable, nextGrabbable, existingAnchor.y, breakForce, breakTorque);
				}
			}
		}
	}

	private void AddHinge(Cuttable parentCuttable, Cuttable newCuttable, float cutYposition, float breakForce = 2000.0f, float breakTorque = 200.0f )
    {
		Vector3 cutPosition = parentCuttable.transform.position;
		cutPosition.y = cutYposition;

		// now make sure the new (hanging) object is hinged to the original object
		Grabbable.GrabbableConfig config;
		config.addHinge = true;
		config.breakForce = breakForce;
		config.breakTorque = breakTorque;
		config.addSpring = false;
		config.addLimits = true;
		config.degradeHingeOverTime = true;

		Grabbable grabbable = newCuttable.GetComponent<Grabbable>();
		grabbable.attachMeshes(parentCuttable.transform, cutPosition, newCuttable.transform.right, config);
    }
}
