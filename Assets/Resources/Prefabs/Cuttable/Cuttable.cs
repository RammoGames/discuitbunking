﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Cuttable : MonoBehaviour
{
	[SerializeField]
	private float m_cutTime = 1.0f;

	[SerializeField]
	private Material m_cutMaterial = null;

	public float cutTime
    {
		get { return m_cutTime; }
	}

    public Material cutMaterial
    {
		get { return m_cutMaterial; }
	}
}
